# frozen_string_literal: true

module UnitHelper
  #
  # Shorthand for `described_class`
  #
  # @return [any] the class being described in the given spec
  #
  def klass
    described_class
  end
end
