# frozen_string_literal: true

RSpec.describe I18n::Ts::Argument do
  let(:instance) { klass.new(name, type: type, typed: typed) }
  let(:name) { "count" }
  let(:typed) { true }

  describe "#initialize" do
    it "accepts a string as a name for the new argument" do
      expect { klass.new("argument_name") }.not_to raise_error
    end

    it "accepts a symbol as a name for the new argument" do
      expect { klass.new(:argument_name) }.not_to raise_error
    end

    it "accepts a string as a type for the new argument" do
      expect { klass.new("argument_name", type: "string") }.not_to raise_error
    end

    it "accepts a symbol as a type for the new argument" do
      expect { klass.new(:name, type: :number) }.not_to raise_error
    end

    it "has a default type DEFAULT_TYPE when none is provided" do
      expect(klass.new(:name).type).to eq(klass::DEFAULT_TYPE)
    end
  end

  describe "#typed_s" do
    context "when no type is provided" do
      it "returns the argument name and default type `<name>: <type>`" do
        expect(klass.new(name).typed_s).to eq("#{name}: #{klass::DEFAULT_TYPE}")
      end
    end

    context "when a type is provided" do
      let(:type) { :any }

      it "returns the argument name and type as `<name>: <type>`" do
        expect(instance.typed_s).to eq("#{name}: #{type}")
      end
    end
  end

  describe "#untyped_s" do
    context "when no type is provided" do
      it "returns the argument name" do
        expect(klass.new(name).untyped_s).to eq(name)
      end
    end

    context "when type `string` is provided" do
      let(:type) { :string }

      it "return the argument name (without type)" do
        expect(instance.untyped_s).to eq(name)
      end
    end
  end

  describe "#to_s" do
    let(:type) { :string }

    context "when `@typed` is true" do
      it "returns the default output of `#typed_s`" do
        expect(instance.to_s).to eq(instance.typed_s)
      end
    end

    context "when `@typed` is false" do
      let(:typed) { false }

      it "return the output of `#untyped_s`" do
        expect(instance.to_s).to eq(instance.untyped_s)
      end
    end
  end
end
