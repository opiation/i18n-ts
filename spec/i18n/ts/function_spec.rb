# frozen_string_literal: true

RSpec.describe I18n::Ts::Function do
  let(:function) do
    klass.new(name, arguments: { first: :number, second: :string },
                    arrow: arrow,
                    blocked: blocked,
                    body: "const counter = 42;\nreturn counter",
                    typed: true)
  end
  let(:arrow) { true }
  let(:blocked) { true }
  let(:name) { "example" }

  describe "#arguments_s" do
    let(:options) do
      { arguments: arguments, named_arguments: named_arguments, typed: typed }
    end

    context "with types" do
      let(:arguments) { { first: :any, second: :number } }
      let(:typed) { true }

      context "with `named_arguments` set to `true`" do
        let(:named_arguments) { true }

        it "returns argument names separated by a comma, wrapped in braces" do
          expect(klass.new(**options).arguments_s).to eq(
            "{ first, second }: { first: any, second: number }"
          )
        end
      end

      context "with `named_arguments` set to `false`" do
        let(:named_arguments) { false }

        it "returns arguments names separated by a comma" do
          expect(klass.new(**options).arguments_s).to eq(
            "first: any, second: number"
          )
        end
      end
    end

    context "without types" do
      let(:arguments) { %i[first second] }
      let(:typed) { false }

      context "with `named_arguments` set to `true`" do
        let(:named_arguments) { true }

        it "returns argument names separated by a comma, wrapped in braces" do
          expect(klass.new(**options).arguments_s).to eq(
            "{ first, second }"
          )
        end
      end

      context "with `named_arguments` set to `false`" do
        let(:named_arguments) { false }

        it "returns arguments names separated by a comma" do
          expect(klass.new(**options).arguments_s).to eq(
            "first, second"
          )
        end
      end
    end
  end

  describe "#arguments?" do
    context "when created with arguments" do
      it "returns `true`" do
        expect(klass.new(arguments: [:first]).arguments?).to eq(true)
      end
    end

    context "when created without arguments" do
      it "returns `false`" do
        expect(klass.new.arguments?).to eq(false)
      end
    end
  end

  describe "#as_arrow" do
    context "when block-wrapped body is requested with `blocked`" do
      ts_func = "({ first, second }: { first: number, second: string }) => " \
                "{ const counter = 42;\nreturn counter }"

      it "returns #{ts_func}" do
        expect(function.as_arrow).to eq(ts_func)
      end
    end

    context "when block-wrapped body has not been requested with `blocked`" do
      let(:blocked) { false }

      ts_func = "({ first, second }: { first: number, second: string }) => " \
                    "const counter = 42;\nreturn counter"

      it "returns #{ts_func}" do
        expect(function.as_arrow).to eq(ts_func)
      end
    end
  end

  describe "#as_function" do
    context "when named" do
      ts_function = "function example({ first, second }: { first: number, " \
                    "second: string }) { const counter = 42;\nreturn counter }"

      it "return #{ts_function}" do
        expect(function.as_function).to eq(ts_function)
      end
    end

    context "when anonymous" do
      let(:name) { nil }

      ts_function = "function ({ first, second }: { first: number, " \
                    "second: string }) { const counter = 42;\nreturn counter }"

      it "return #{ts_function}" do
        expect(function.as_function).to eq(ts_function)
      end
    end
  end

  describe "#to_s" do
    context "when `arrow` function requested" do
      let(:arrow) { true }

      it "returns the same result as `#as_arrow`" do
        expect(function.to_s).to eq(function.as_arrow)
      end
    end

    context "when `arrow` function has not been requested" do
      let(:arrow) { false }

      it "returns the same result as `#as_function`" do
        expect(function.to_s).to eq(function.as_function)
      end
    end
  end
end
