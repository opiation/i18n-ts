# frozen_string_literal: true

RSpec.describe I18n::Ts do
  it "has a version number" do
    expect(I18n::Ts::VERSION).not_to be nil
  end
end
