# frozen_string_literal: true

namespace :i18n do
  namespace :ts do
    desc "Export Typescript i18n messages"
    task :export_safe, :locale_names, :root_locale do |_, _options|
      puts locale_paths
    end

    def locale_paths
      Dir.glob("./spec/i18n/ts/*.rb")
    end
  end
end
