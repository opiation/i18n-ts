# frozen_string_literal: true

module I18n
  #
  # Automatically loads i18n:ts tasks into the consuming project's tasks
  #
  class Railtie < Rails::Railtie
    rake_tasks do
      load "tasks/i18n/ts/export.rake"
    end
  end
end
