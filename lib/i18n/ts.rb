# frozen_string_literal: true

require "i18n/ts/argument"
require "i18n/ts/function"
require "i18n/ts/version"

require "i18n/railtie" if defined?(Rails)

module I18n
  module Ts
    class Error < StandardError; end
    # Your code goes here...
  end
end
