# frozen_string_literal: true

module I18n
  module Ts
    #
    # Represents an argument used in a given Typescript/ECMAscript function
    #
    class Argument
      attr_accessor :name, :type, :typed

      DEFAULT_TYPE = :any
      TYPE_SEPARATOR = ": "

      def initialize(name, type: DEFAULT_TYPE, typed: true)
        @name = name
        @type = type
        @typed = typed
      end

      #
      # Returns a type-annotated string representation of this argument
      #
      # @return [String]
      #
      def typed_s
        "#{@name}#{TYPE_SEPARATOR}#{@type}"
      end

      #
      # Returns a string representation of this argument without any type
      # annotation.  This is just the argument name, essentially
      #
      # @return [String]
      #
      def untyped_s
        @name.to_s
      end

      #
      # Returns a string represent of this argument.  Used the type-annotated
      # form if `typed?` is true.
      #
      # @return [String]
      #
      def to_s
        if typed?
          typed_s
        else
          untyped_s
        end
      end

      alias typed? typed
    end
  end
end
