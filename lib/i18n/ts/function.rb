# frozen_string_literal: true

require_relative "./argument"

module I18n
  module Ts
    #
    # Represents a function in proper Typescript or ECMAscript
    #
    class Function
      KV_SEPARATOR = ":"
      PARAMETER_SEPARATOR = ", "
      TOKEN = "function"

      class << self
        def parse_arguments(args, typed = true)
          if args.is_a?(Hash)
            args.map do |name, type|
              Argument.new(name, type: type, typed: typed)
            end
          elsif args.respond_to?(:map)
            args.map do |argument|
              return argument if argument.is_a?(Argument)

              Argument.new(argument, typed: typed)
            end
          end
        end
      end

      attr_reader :arguments, :arrow, :body, :blocked, :name, :named_arguments,
                  :typed

      def initialize(name = nil,
                     arguments: [],
                     arrow: true,
                     body: "",
                     blocked: false,
                     named_arguments: true,
                     typed: true)
        @arguments = self.class.parse_arguments(arguments)
        @arrow = arrow
        @body = body
        @blocked = blocked
        @name = name
        @named_arguments = named_arguments
        @typed = typed
      end

      #
      # Returns a string representation of the expected arguments for this
      # Typescript function.  If no arguments are required, this returns an
      # empty string.
      #
      # @return [String]
      #
      def arguments_s
        args = @arguments.map(&:name).join(PARAMETER_SEPARATOR)
        args = "{ #{args} }" if @named_arguments
        return args unless @typed

        types = @arguments.map(&:to_s).join(PARAMETER_SEPARATOR)
        types = "{ #{types} }" if @named_arguments
        return types unless @named_arguments

        "#{args}#{KV_SEPARATOR} #{types}"
      end

      #
      # Does the generated Typescript function require arguments?
      #
      # @return [Boolean]
      #
      def arguments?
        !@arguments.empty?
      end

      #
      # Returns the arrow function form of this function
      #
      # @return [String]
      #
      def as_arrow
        if blocked?
          "#{signature} => { #{@body} }"
        else
          "#{signature} => #{@body}"
        end
      end

      #
      # Returns the function form of this function, including the `function`
      # token
      #
      # @return [String]
      #
      def as_function
        if named?
          "#{TOKEN} #{@name || ''}#{signature} { #{@body} }"
        else
          "#{TOKEN} #{signature} { #{@body} }"
        end
      end

      #
      # Is this function named?  This is ignored when generating an arrow
      # function
      #
      # @return [Boolean]
      #
      def named?
        @name && !@name.empty?
      end

      #
      # Returns a string representation of this function's parameter signature
      #
      # @return [String]
      #
      def signature
        "(#{arguments_s})"
      end

      #
      # Return a string representation of the Typescript function.
      #
      # @return [String]
      #
      def to_s
        if arrow?
          as_arrow
        else
          as_function
        end
      end

      alias arrow? arrow
      alias blocked? blocked
      alias typed? typed
    end
  end
end
